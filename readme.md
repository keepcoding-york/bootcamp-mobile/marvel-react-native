![HEADER](readme/header.png)
# Marvel React Native

Educational project based on React Native. Powered by [Redux](https://redux.js.org), [RxJS](https://rxjs-dev.firebaseapp.com) and [Redux Observable](https://redux-observable.js.org) with a [Clean Architecture](https://es.wikipedia.org/wiki/Robert_C._Martin) approach.

![Technologies](readme/technologies.png)

![Screens](readme/screens.png)

## ✨ Features ✨
- Listed characters
- Character details
- Add character (simulated)

## ⛰ Structure ⛰

![Structure](readme/structure.png)

## 🚧 Roadmap 🚧

- [ ] List with scroll infinite
- [ ] Transition morph List to Detail
- [ ] Detail animation intro
- [ ] Navigation with Redux Action

