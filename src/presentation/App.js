import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import {
  Router, Scene, Stack,
} from 'react-native-router-flux';
import { configureStore } from '../data/redux';
import ButtonNavigation from './components/buttonNavigation/ButtonNavigation';
import {
  Main,
  Detail,
  Editor,
  MAIN_KEY,
  DETAIL_KEY,
  EDITOR_KEY,
} from './scenes';

const { store, persistor } = configureStore();

const App = () => (
  <Provider store={store}>
    <PersistGate
      persistor={persistor}
      loading={null}
    >
      <Router
        navigationBarStyle={{
          backgroundColor: '#E23636',
        }}
        titleStyle={{
          color: '#fff',
        }}
        backButtonTintColor="white"
        backButtonTextStyle={{ color: 'white' }}
      >
        <Stack key="root">
          <Scene
            key={MAIN_KEY}
            component={Main}
            title="Marvel"
            renderRightButton={(
              <ButtonNavigation
                scene={EDITOR_KEY}
                title="Add hero"
                children="+"
              />
)}
            initial
          />
          <Scene
            key={DETAIL_KEY}
            component={Detail}
          />
          <Scene
            key={EDITOR_KEY}
            component={Editor}
          />
        </Stack>
      </Router>
    </PersistGate>
  </Provider>
);

export default App;
