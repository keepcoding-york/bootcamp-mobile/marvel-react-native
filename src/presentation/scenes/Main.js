import React from 'react';
import { CharacterList } from '../components/character';

const Main = () => (<CharacterList />);

export default Main;
