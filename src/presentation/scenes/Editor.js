import React from 'react';
import {
  ScrollView,
} from 'react-native';
import { CharacterEditor } from '../components/character';

const Editor = () => (<ScrollView><CharacterEditor /></ScrollView>);

export default Editor;
