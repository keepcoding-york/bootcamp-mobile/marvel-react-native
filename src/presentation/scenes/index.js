export const DETAIL_KEY = 'DETAIL_KEY';
export const MAIN_KEY = 'MAIN_KEY';
export const EDITOR_KEY = 'EDITOR_KEY';

export Detail from './Detail';
export Main from './Main';
export Editor from './Editor';
