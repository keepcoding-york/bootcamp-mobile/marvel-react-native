import React from 'react';
import { CharacterDetail } from '../components/character';

const Detail = () => (<CharacterDetail />);

export default Detail;
