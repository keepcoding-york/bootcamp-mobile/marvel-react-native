import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

const propTypes = {
  children: PropTypes.any,
  scene: PropTypes.string.isRequired,
  title: PropTypes.string,
};

const ButtonNavigation = ({ scene, title, children }) => (
  <TouchableOpacity
    style={{ padding: 10 }}
    onPress={() => Actions[scene].call({ title })}
  >
    <Text style={{
      color: 'white',
      fontWeight: '300',
      fontSize: 25,
      lineHeight: 25,
    }}
    >
      {children}
    </Text>
  </TouchableOpacity>
);


ButtonNavigation.propTypes = propTypes;

export default ButtonNavigation;
