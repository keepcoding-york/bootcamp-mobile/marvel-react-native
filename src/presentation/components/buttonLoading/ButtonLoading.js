import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from 'react-native';
import Spinner from 'react-native-spinkit';
import styles from './buttonLoadingStyle';

const propTypes = {
  isFetching: PropTypes.bool,
  label: PropTypes.string,
  onPress: PropTypes.any,
};

const defaultProps = {
  isFetching: false,
  label: 'Save',
  onPress: () => {
  },
};

class ButtonLoading extends Component {
  onPress() {
    const { isFetching, onPress } = this.props;
    if (!isFetching) {
      onPress();
    }
  }

  renderContent() {
    const { isFetching, label } = this.props;
    if (isFetching) {
      return (
        <Spinner
          color="#FFF"
          size={20}
          type="ChasingDots"
        />
      );
    }
    return <Text style={styles.buttonText}>{label}</Text>;
  }

  render() {
    const { isFetching } = this.props;
    return (
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => this.onPress()}
        activeOpacity={isFetching ? 1 : 0.2}
      >
        {this.renderContent()}
      </TouchableOpacity>
    );
  }
}

ButtonLoading.propTypes = propTypes;
ButtonLoading.defaultProps = defaultProps;

export default ButtonLoading;
