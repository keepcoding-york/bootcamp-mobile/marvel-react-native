import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  buttonContainer: {
    backgroundColor: '#E23636',
    padding: 20,
    paddingVertical: 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 14,
  },
  buttonText: {
    fontWeight: '300',
    color: 'white',
    fontSize: 18,
    textTransform: 'uppercase',
  },
});
