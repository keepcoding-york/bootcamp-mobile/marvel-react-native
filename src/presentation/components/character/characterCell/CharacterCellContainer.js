import { connect } from 'react-redux';
import {
  characterSelectedAction,
} from '../../../../data/redux';
import CharacterCell from './CharacterCell';


const mapDispatchToProps = dispatch => ({
  characterSelectedAction: id => dispatch(characterSelectedAction(id)),
});

export default connect(null, mapDispatchToProps)(CharacterCell);
