import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  cellContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 180,
  },
  backgroundGradient: {
    position: 'absolute',
    margin: 15,
    marginLeft: 30,
    marginRight: 20,
    width: '82%',
    height: 150,
    backgroundColor: '#E23636',
    borderRadius: 15,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    opacity: 0.3,
  },
  backgroundColor: {
    position: 'absolute',
    margin: 15,
    marginLeft: 30,
    marginRight: 20,
    backgroundColor: '#E23636',
    borderRadius: 15,
    height: 150,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  photo: {
    width: 120,
    height: 120,
    borderRadius: 15,
    margin: 10,
    borderColor: '#e23636',
    borderWidth: 0,
    zIndex: 10,
    backgroundColor: 'black',
  },
  name: {
    fontSize: 24,
    fontWeight: '800',
    color: '#fff',
    lineHeight: 24,
    paddingRight: 10,
  },
  separator: {
    height: 2,
    backgroundColor: '#fff',
    left: -10,
    width: '15%',
    marginTop: 5,
    marginBottom: 10,
  },
  description: {
    fontSize: 16,
    fontWeight: '300',
    color: '#fff',
    lineHeight: 16,
    paddingRight: 10,
  },
  cellContentTextAnimation: {
    flex: 1,
    margin: 40,
    marginLeft: 20,
  },
});
