import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './characterCellStyle';
import { DETAIL_KEY } from '../../../scenes';

const defaultProps = {
  name: '',
  description: '',
  thumbnail: '../../../../resources/backgroundGradient.jpg',
};

const propTypes = {
  characterSelectedAction: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  description: PropTypes.string,
  thumbnail: PropTypes.string,
};

class CharacterCell extends Component {
  constructor(props) {
    super(props);
    this.backgroundGradient = '../../../../resources/backgroundGradient.jpg';
    this.state = {
      photoAnimation: new Animated.Value(20),
      cellContentTextAnimation: new Animated.Value(0),
    };
  }

  componentDidMount() {
    this.animationInitial();
  }

  onPressButton() {
    const { characterSelectedAction, id, name } = this.props;
    characterSelectedAction(id);
    Actions[DETAIL_KEY].call({ title: name });
  }

  animationInitial() {
    const { photoAnimation, cellContentTextAnimation } = this.state;
    Animated.timing(
      photoAnimation,
      {
        toValue: 0,
        easing: Easing.out(Easing.exp),
        duration: 1500,
      },
    )
      .start();
    Animated.timing(
      cellContentTextAnimation,
      {
        toValue: 1,
        easing: Easing.out(Easing.exp),
        duration: 2500,
      },
    )
      .start();
  }


  render() {
    const { thumbnail, name, description } = this.props;
    const { cellContentTextAnimation, photoAnimation } = this.state;
    return (
      <TouchableOpacity
        onPress={() => this.onPressButton()}
        activeOpacity={0.9}
      >
        <View
          style={styles.cellContainer}
        >
          <View style={styles.backgroundColor} />
          <Image
            style={styles.backgroundGradient}
            source={require('../../../../resources/backgroundGradient.jpg')}
            resizeMode="cover"
          />
          <Animated.Image
            source={{ uri: thumbnail }}
            style={[styles.photo, { left: photoAnimation }]}
            resizeMode="cover"
          />
          <Animated.View
            style={[styles.cellContentTextAnimation, { opacity: cellContentTextAnimation }]}
          >

            <Text
              numberOfLines={1}
              style={styles.name}
            >
              {name}
            </Text>
            <View style={styles.separator} />
            <Text
              numberOfLines={2}
              style={styles.description}
            >
              {description}
            </Text>
          </Animated.View>
        </View>
      </TouchableOpacity>
    );
  }
}

CharacterCell.defaultProps = defaultProps;
CharacterCell.propTypes = propTypes;

export default CharacterCell;
