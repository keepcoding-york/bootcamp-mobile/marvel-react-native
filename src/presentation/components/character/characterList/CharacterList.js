import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  View, FlatList,
} from 'react-native';
import styles from './characterListStyles';
import { CharacterCell } from '../index';

const defaultProps = {
  characterFetchAction: () => {},
  isFetching: true,
  list: [],
};

const propTypes = {
  characterFetchAction: PropTypes.func,
  isFetching: PropTypes.bool,
  list: PropTypes.array,
};

class CharacterList extends Component {
  componentDidMount() {
    const { characterFetchAction } = this.props;
    characterFetchAction();
  }

  renderItem(item) {
    const {
      thumbnail, name, description, id,
    } = item;
    return (
      <CharacterCell
        thumbnail={thumbnail}
        name={name}
        description={description}
        id={id}
      />
    );
  }


  render() {
    const { list, isFetching } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={list}
          renderItem={({ item }) => this.renderItem(item)}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}

CharacterList.defaultProps = defaultProps;
CharacterList.propTypes = propTypes;

export default CharacterList;
