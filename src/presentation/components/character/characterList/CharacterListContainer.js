import { connect } from 'react-redux';
import {
  characterListSelector,
  characterIsFetchingSelector,
} from '../../../../data/redux';
import {
  characterFetchAction,
} from '../../../../data/redux';

import CharacterList from './CharacterList';

const mapStateToProps = state => ({
  list: characterListSelector(state),
  isFetching: characterIsFetchingSelector(state),
});

const mapDispatchToProps = dispatch => ({
  characterFetchAction: params => dispatch(characterFetchAction(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CharacterList);
