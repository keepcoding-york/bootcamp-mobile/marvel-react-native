import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  backgroundColor: {
    position: 'absolute',
    margin: 10,
    backgroundColor: '#E23636',
    borderRadius: 15,
    height: 150,
    left: 0,
    bottom: 0,
    right: 0,
  },
  photo: {
    width: '100%',
    height: 300,
    borderRadius: 15,
    borderColor: '#e23636',
    borderWidth: 0,
    zIndex: 10,
    backgroundColor: 'black',
  },
  name: {
    fontSize: 32,
    fontWeight: '800',
    color: '#000',
    lineHeight: 32,
  },
  separator: {
    height: 4,
    backgroundColor: '#e23636',
    left: -10,
    width: '15%',
    marginTop: 5,
    marginBottom: 10,
  },
  description: {
    fontSize: 16,
    fontWeight: '300',
    color: '#333',
    lineHeight: 20,
  },
  contentText: {
    padding: 30,
    paddingTop: 15,
  },
  contentImage: {
    padding: 30,
    paddingTop: 20,
  },
  container: {
    padding: 10,
    paddingTop: 0,
    backgroundColor: '#ffffff',
    height: '100%',
  },
});
