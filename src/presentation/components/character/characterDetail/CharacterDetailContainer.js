import { connect } from 'react-redux';
import {
  characterIsFetchingSelector, characterItemSelector,
} from '../../../../data/redux';

import CharacterDetail from './CharacterDetail';

const mapStateToProps = state => ({
  item: characterItemSelector(state),
  isFetching: characterIsFetchingSelector(state),
});

export default connect(mapStateToProps)(CharacterDetail);
