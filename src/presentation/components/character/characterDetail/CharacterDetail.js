import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Text,
  View,
  ScrollView,
} from 'react-native';
import styles from './characterDetailStyle';

const defaultProps = {
  description: '',
  name: '',
  thumbnail: '../../../../resources/backgroundGradient.jpg',
};

const propTypes = {
  item: PropTypes.object.isRequired,
  description: PropTypes.string,
  name: PropTypes.string,
  thumbnail: PropTypes.string,
};

class CharacterDetail extends Component {
  constructor(props) {
    super(props);
    this.backgroundGradient = '../../../../resources/backgroundGradient.jpg';
  }

  render() {
    const { item } = this.props;
    const { thumbnail, name, description } = item;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.contentImage}>
            <Image
              source={{ uri: thumbnail }}
              style={styles.photo}
              resizeMode="cover"
            />
            <View style={styles.backgroundColor}/>
          </View>
          <View
            style={styles.contentText}
          >
            <Text
              style={styles.name}
            >
              {name}
            </Text>
            <View style={styles.separator}/>
            <Text
              style={styles.description}
            >
              {description}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

CharacterDetail.defaultProps = defaultProps;
CharacterDetail.propTypes = propTypes;

export default CharacterDetail;
