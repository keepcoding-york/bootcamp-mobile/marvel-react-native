import { connect } from 'react-redux';
import {
  characterSubmitAction,
} from '../../../../data/redux';

import CharacterEditor from './CharacterEditor';

const mapDispatchToProps = dispatch => ({
  characterSubmitAction: character => dispatch(characterSubmitAction(character)),
});

export default connect(null, mapDispatchToProps)(CharacterEditor);
