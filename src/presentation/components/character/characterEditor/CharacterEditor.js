import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  View,
  Alert,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import styles from './characterEditorStyle';
import FieldItem from '../../fieldItem/FieldItem';
import ButtonLoading from '../../buttonLoading/ButtonLoading';

const propTypes = {
  characterSubmitAction: PropTypes.func,
  isFetching: PropTypes.bool,
};

const defaultProps = {
  characterSubmitAction: () => {},
  isFetching: false,
};

class CharacterEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.options = {
      title: 'Select image',
      maxWidth: 640,
      maxHeight: 640,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
  }


  onImagePickerTapped() {
    ImagePicker.showImagePicker(this.options, (response) => {
      if (response.uri && response.data) {
        const preview = { uri: response.uri };
        const data = `data:image/jpeg;base64,${response.data}`;
        this.setState({
          image: {
            preview,
            data,
          },
        });
      }
    });
  }

  onSubmit() {
    const { characterSubmitAction } = this.props;
    if (this.validateForm()) {
      const { image, name, description } = this.state;
      const data = {
        name,
        description,
        thumbnail: image.data,
      };
      characterSubmitAction(data);
      Actions.pop();
    } else {
      Alert.alert('Attention', 'Complete all fields.');
    }
  }


  validateForm() {
    const { image, name, description } = this.state;
    return !!(image && name && description);
  }

  renderTextInput(label, key, placeholder = '', multiLine, numberOfLines) {
    return (
      <FieldItem
        multiLine={multiLine}
        numberOfLines={numberOfLines}
        label={label}
        value={this.state[key]}
        onChangeText={v => this.setState({ [key]: v })}
        placeholder={placeholder}
      />
    );
  }

  renderImageInput() {
    const { image } = this.state;
    const imageUri = image ? image.preview : null;
    const imageLabel = image ? 'Press to choose another image' : 'Press to choose image';
    return (
      <View style={{ marginTop: 20 }}>
        <TouchableOpacity
          style={styles.imageContainer}
          onPress={() => this.onImagePickerTapped()}
        >
          <Image
            source={imageUri}
            style={styles.image}
            resizeMode="cover"
          />
          <Text style={styles.imageText}>{imageLabel}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const { isFetching } = this.props;
    return (
      <View>
        <View style={{ margin: 20 }}>
          {this.renderTextInput('Name', 'name', 'Deadpool')}
        </View>
        <View style={{ margin: 20 }}>
          {this.renderTextInput('Description', 'description', 'Deadpool is sexy', true, 4)}
        </View>
        <View style={{
          paddingHorizontal: 20,
          paddingBottom: 40,
        }}
        >
          {this.renderImageInput()}
        </View>
        <View style={{ paddingHorizontal: 20, paddingBottom: 20 }}>
          <ButtonLoading
            label={'Save'.toUpperCase()}
            onPress={() => this.onSubmit()}
            isFetching={isFetching}
          />
        </View>
      </View>
    );
  }
}

CharacterEditor.propTypes = propTypes;

CharacterEditor.defaultProps = defaultProps;

export default CharacterEditor;
