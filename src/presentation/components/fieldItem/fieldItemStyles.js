import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  label: {
    color: '#e23636',
    fontSize: 16,
    marginBottom: 5,
    paddingHorizontal: 10,
  },
  textInput: {
    color: '#1d1d1d',
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#2e2e2e',
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    paddingVertical: 6,
    paddingHorizontal: 10,
    borderRadius: 10,
  },
  textInputFocus: {
    color: '#1d1d1d',
    fontSize: 18,
    borderWidth: 1,
    borderColor: '#e23636',
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    paddingVertical: 6,
    paddingHorizontal: 10,
    borderRadius: 10,
  },
});
