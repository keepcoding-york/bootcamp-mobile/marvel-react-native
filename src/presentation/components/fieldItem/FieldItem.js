import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, Text } from 'react-native';
import styles from './fieldItemStyles';

const defaultProps = {
  containerStyle: {},
  inputStyle: {},
  inputFocusStyle: {},
  labelStyle: {},
  label: '',
  onChangeText: () => {
  },
  placeholder: '',
  value: '',
  placeholderTextColor: '#b1b1b1',
  multiLine: false,
  numberOfLines: 1,
};

const propTypes = {
  containerStyle: PropTypes.object,
  labelStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  inputFocusStyle: PropTypes.object,
  label: PropTypes.string,
  onChangeText: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  multiLine: PropTypes.bool,
  numberOfLines: PropTypes.number,
};

class FieldItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
    };
  }

  onFocus() {
    this.setState({
      isFocused: true,
    });
  }

  onBlur() {
    this.setState({
      isFocused: false,
    });
  }

  render() {
    const {
      containerStyle,
      labelStyle,
      label,
      onChangeText,
      value,
      inputStyle,
      placeholder,
      placeholderTextColor,
      inputFocusStyle,
      multiLine,
      numberOfLines,
    } = this.props;
    const { isFocused } = this.state;
    return (
      <View style={[containerStyle]}>
        <Text
          style={[styles.label, labelStyle]}
        >
          {label}
        </Text>
        <TextInput
          multiline={multiLine || false}
          numberOfLines={numberOfLines || 1}
          onChangeText={valueInput => onChangeText(valueInput)}
          value={value}
          style={isFocused ? [[styles.textInputFocus, inputStyle]] : [styles.textInput, inputFocusStyle]}
          onFocus={() => this.onFocus()}
          onBlur={() => this.onBlur()}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor}
        />
      </View>
    );
  }
}


FieldItem.propTypes = propTypes;
FieldItem.defaultProps = defaultProps;

export default FieldItem;
