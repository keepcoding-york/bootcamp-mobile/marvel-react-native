import { combineEpics } from 'redux-observable';
import {
  charactersFetchEpic,
  characterSelectedEpic,
  characterSubmitEpic,
} from './character';

export const rootEpic = combineEpics(
  charactersFetchEpic,
  characterSelectedEpic,
  characterSubmitEpic,
);
