import {
  map, mergeMap,
} from 'rxjs/operators';
import { ofType } from 'redux-observable';
import {
  characterSetItemAction,
  characterSetListAction,
  characterAddAction,
} from './actions';
import * as types from './types';
import { characterMapper } from './model';
import { getCharactersApi$ } from '../../net';
import { characterListSelector } from './selectors';


export const charactersFetchEpic = action$ => action$.pipe(
  ofType(types.CHARACTERS_FETCH),
  mergeMap(action => getCharactersApi$(action.payload)
    .pipe(
      map(response => characterMapper(response)),
      map(characters => characterSetListAction(characters)),
    )),
);

export const characterSelectedEpic = (action$, state) => action$.pipe(
  ofType(types.CHARACTER_SELECTED),
  map((action) => {
    const list = characterListSelector(state.value);
    const id = action.payload;
    // TODO Check option id not valid
    return list.filter(item => id === item.id)[0];
  }),
  map(item => characterSetItemAction(item)),
);

export const characterSubmitEpic = action$ => action$.pipe(
  ofType(types.CHARACTER_SUBMIT),
  map((action) => {
    const character = action.payload;
    return { ...character, id: 9999999 };
  }),
  map(character => characterAddAction(character)),
);
