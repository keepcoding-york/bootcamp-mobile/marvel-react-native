import * as types from './types';

export const characterFetchAction = params => ({
  type: types.CHARACTERS_FETCH,
  payload: params,
});

export const characterSetFetchingAction = isFetching => ({
  type: types.CHARACTER_SET_FETCHING,
  payload: isFetching,
});

export const characterSetListAction = characters => ({
  type: types.CHARACTER_UPDATE_LIST,
  payload: characters,
});

export const characterSetItemAction = character => ({
  type: types.CHARACTER_SET_ITEM,
  payload: character,
});

export const characterSelectedAction = id => ({
  type: types.CHARACTER_SELECTED,
  payload: id,
});

export const characterSubmitAction = character => ({
  type: types.CHARACTER_SUBMIT,
  payload: character,
});

export const characterAddAction = character => ({
  type: types.CHARACTER_ADD,
  payload: character,
});
