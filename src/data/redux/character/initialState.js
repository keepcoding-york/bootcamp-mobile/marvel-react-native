const initialState = {
  isFetching: false,
  list: [],
  item: null,
};

export default initialState;
