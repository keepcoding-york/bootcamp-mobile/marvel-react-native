import { createSelector } from 'reselect';

export const characterSelector = state => state.character;

export const characterListSelector = createSelector(
  characterSelector,
  character => character.list,
);

export const characterItemSelector = createSelector(
  characterSelector,
  character => character.item,
);

export const characterIsFetchingSelector = createSelector(
  characterSelector,
  character => character.isFetching,
);
