export const characterMapper = response => response.data.results.map((character) => {
  const { thumbnail } = character;
  const image = `${thumbnail.path}.${thumbnail.extension}`;
  return {
    ...character,
    thumbnail: image,
  };
});
