export const CHARACTER_UPDATE_LIST = 'CHARACTER_UPDATE_LIST';
export const CHARACTER_SET_FETCHING = 'CHARACTER_SET_FETCHING';
export const CHARACTER_SET_ITEM = 'CHARACTER_SET_ITEM';
export const CHARACTERS_FETCH = 'CHARACTERS_FETCH';
export const CHARACTER_SELECTED = 'CHARACTER_SELECTED';
export const CHARACTER_SUBMIT = 'CHARACTER_SUBMIT';
export const CHARACTER_ADD = 'CHARACTER_ADD';
