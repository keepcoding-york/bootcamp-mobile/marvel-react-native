import * as types from './types';
import initialState from './initialState';

const characterReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.CHARACTER_SET_FETCHING:
      return {
        ...state,
        isFetching: payload,
      };

    case types.CHARACTER_UPDATE_LIST:
      return {
        ...state,
        list: payload,
      };

    case types.CHARACTER_SET_ITEM:
      return {
        ...state,
        item: payload,
      };

    case types.CHARACTER_ADD:
      return {
        ...state,
        list: [...state.list, payload],
      };

    default:
      return state;
  }
};

export default characterReducer;
