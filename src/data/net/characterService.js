import { ajax } from 'rxjs/ajax';

const referer = 'york.marvel.com';
const baseUrl = 'https://gateway.marvel.com:443/v1/public/';
const apiKey = '48330f61c640c5b0e5066d3c33cbd0d6';
const headers = {
  Referer: referer,
};

const configUrl = query => `${baseUrl}${query}?apikey=${apiKey}`;

const OFFSET = 0;
const LIMIT = 20;
const getParams = {
  limit: LIMIT,
  offset: OFFSET,
};
const GET_CHARACTERS_ENDPOINT = 'characters';

export const getCharactersApi$ = ({ limit = getParams.limit, offset = getParams.offset } = getParams) => ajax.getJSON(`${configUrl(GET_CHARACTERS_ENDPOINT)}&limit=${limit}&offset=${offset}`, headers);
